<?php

/* Template name: Rekli su o nama */

get_header(); ?>

<div id="content" style="position:relative;">
    <?php
    get_template_part( 'content', 'top-image' );
    get_template_part( 'content', 'testimonials-big' );
    get_template_part( 'content', 'side-testimonials' );
    ?>
</div>
<div style="clear:both;"></div>

<?php get_footer();
