<div class="left_section">
    <div class="osoblje" >
        <?php
        if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
            ?>
            Testimonials
            <?php
        } else {
            ?>
            Rekli su o nama
            <?php
        }
        ?>
    </div>
    <div style="color:#888;padding: 20px 20px 4px 30px;text-align:justify;position:relative; font-size:12px; line-height:18px;margin: 0 20px 0 0;" class="news_text">
    	<span class="quote_text"></span>
         <?php
         if( have_posts() ) :
             while( have_posts() ) :
                 the_post();
                 ?>
                 <div class="news_name">
                     <?php the_title(); ?>
                 </div>
                 <div>
                     <?php echo get_post_meta( $post->ID, '_testimonials_value_key', true ); ?>
                 </div>
                 <?php the_content();
             endwhile;
             wp_reset_postdata();
         endif;
         ?>
    </div>
</div>
