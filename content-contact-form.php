<?php require_once 'contact-form.php'; ?>

<div class="left_section">
    <div class="contact">
        <div class="white_arrow"></div>
        <div class="ask_us">
        	<?php
            if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
                ?>
                <span style="margin:0px 0 10px 0;"><img src="<?php bloginfo('template_url'); ?>/img/pitajte_nas_en.png" width="603" height="45" />
                <?php
            } else {
                ?>
                <span style="margin:0px 0 10px 0;"><img src="<?php bloginfo('template_url'); ?>/img/pitajte_nas_rs2.png" width="603" height="45" />
                <?php
            }
            ?>
        	</span>
            <?php
            if( have_posts() ) :
                while( have_posts() ) :
                    the_post();
                    the_content();
                endwhile;
            endif;
            ?>
        </div>
        <form class="form" action="#" method="post">
        	<table border="0" cellspacing="0" cellpadding="0" class="contact_form">
                <tr>
                    <td rowspan="8" valign="top" style="border:none;">
                        <textarea name="message" id="message" placeholder="<?php echo ( isset($_GET['lang']) && $_GET['lang'] == 'en' ) ? 'Your question or comment*' : 'Vaše pitanje, kritika ili komentar*'; ?>"></textarea>
                    </td>
                    <td style="border:none;"><input name="fullname" id="name" type="text" placeholder="<?php echo ( isset($_GET['lang']) && $_GET['lang'] == 'en' ) ? 'Name:*' : 'Ime i prezime:*'; ?>" class="obavezno" /></td>
                </tr>
                <tr>
                    <td style="border:none;"><input name="company" id="company" type="text" placeholder="<?php echo ( isset($_GET['lang']) && $_GET['lang'] == 'en' ) ? 'Company:*' : 'Kompanija:*'; ?>" class="obavezno"  /></td>
                </tr>
                <tr>
                    <td style="border:none;"><input name="street_number" id="street_number" type="text" placeholder="<?php echo ( isset($_GET['lang']) && $_GET['lang'] == 'en' ) ? 'Street number:' : 'Ulica i broj:'; ?>" /></td>
                </tr>
                <tr>
                    <td><input name="city" id="city" type="text" placeholder="<?php echo ( isset($_GET['lang']) && $_GET['lang'] == 'en' ) ? 'City:' : 'Grad:'; ?>" /></td>
                </tr>
                <tr>
                    <td style="border:none;">
                    <input name="phone" id="phone" type="text" placeholder="<?php echo ( isset($_GET['lang']) && $_GET['lang'] == 'en' ) ? 'Phone:' : 'Kontakt telefon:'; ?>" /></td>
                </tr>
                <tr>
                    <td style="border:none;"><input name="fax" id="fax" type="text" placeholder="<?php echo ( isset($_GET['lang']) && $_GET['lang'] == 'en' ) ? 'Fax:' : 'Faks:'; ?>"/></td>
                </tr>
                <tr>
                    <td style="border:none;"><input name="email" id="email" type="text" placeholder="<?php echo ( isset($_GET['lang']) && $_GET['lang'] == 'en' ) ? 'E-mail:' : 'E-mail adresa:*'; ?>" class="obavezno"  /></td>
                </tr>
                <tr>
                    <td style="border:none;"><input name="web_address" id="site" type="text" placeholder="<?php echo ( isset($_GET['lang']) && $_GET['lang'] == 'en' ) ? 'Internet website:' : 'Internet strana kompanije:'; ?>" /></td>
                </tr>
        	</table>
            <div class="error" style="display:none"></div>
            <div class="success" style="display:none"></div>
        	<button type="btn_submit" name="btn_submit" class="<?php echo ( isset($_GET['lang']) && $_GET['lang'] == 'en' ) ? 'btn_send_en' : 'btn_send_rs'; ?>"></button>
        </form>
    </div>
</div>
