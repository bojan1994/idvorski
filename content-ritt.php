<div class="osoblje" >
    <?php wp_title( '', true, '' ); ?>
</div>
<div>
    <div class="search_holder">
        <form action="#" method="get">
            <div class="search_section_item">
                <div class="search_section_item_name">
                    Pretraga: Ukucajte termin ili datum
                </div>
                <div class="search_section_item_content">
                    <input type="text" name="search_field_search" onkeyup="set_search_string(this)" />
                </div>
            </div>
            <div class="search_section_item">
                <div class="search_section_item_name">
                    Broj rezultata po strani
                </div>
                <div class="search_section_item_content">
                    <select name="search_field_per_page" onchange="change_number_per_page(this)">
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                    </select>
                </div>
            </div>
            <div class="search_section_item">
                <div class="search_section_item_name">
                    Sortiranje
                </div>
                <div class="search_section_item_content">
                    <select name="search_field_sort" onchange="search_field_sort_change(this)">
                        <option value="DESC">Po broju potvrde opadajuce</option>
                        <option value="ASC">Po broju potvrde rastuće</option>
                    </select>
                </div>
            </div>
            <input type="hidden" name="page" value="<?php echo get_the_ID(); ?>">
        </form>
        <div style="clear:both;"></div>
    </div>
</div>
<div class="ritt_table">
    <div class="text_about" id="ritt_search"></div>
</div>
