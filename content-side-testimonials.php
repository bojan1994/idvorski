<div class="right_section">
    <div class="box_news">
        <div class="title">
            <a href="<?php echo get_permalink( get_page_by_title( 'Rekli su o nama' ) ); ?>">
                <?php
                if(isset($_GET['lang']) && $_GET['lang']) {
                    ?>
                    <img src="<?php bloginfo('template_url'); ?>/img/reference.news.en.jpg" width="350" height="41" />
                    <?php
                } else {
                    ?>
                    <img src="<?php bloginfo('template_url'); ?>/img/reference.news.rs.jpg" width="350" height="41" />
                    <?php
                }
                ?>
            </a>
        </div>
        <div class="text_section_news" id="text_section_news">
            <div class="arrow_brown">
                  <a href="<?php echo get_permalink( get_page_by_title( 'Rekli su o nama' ) ); ?>" class="small_arrow"></a>
            </div>
            <div class="text_box_blue">
                <?php
                $args = array(
                    'post_type' => 'testimonials',
                );
                $query = new WP_Query( $args );
                if( $query->have_posts() ) :
                    while( $query->have_posts() ) :
                        $query->the_post();
                        ?>
                        <div class="small_box" style="height:auto;padding:0px 0px 10px 0px">
                            <div class="arrow_small">
                                <a href="<?php the_permalink(); ?>" class="small_arrow"></a>
                            </div>
                            <a href="<?php the_permalink(); ?>">
        						<img src="<?php bloginfo('template_url'); ?>/img/quote_news.jpg" style="float:left;padding:0px 10px 0px 0px;"  />
                            </a>
                            <div style="float:left;text-align:left;width:200px;">
                                <a href="<?php the_permalink(); ?>">
        						 	<?php the_title(); ?> - <?php echo get_post_meta( $post->ID, '_testimonials_value_key', true ); ?>
                                </a>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                        <?php
                    endwhile;
                    wp_reset_postdata();
                endif;
                ?>
            </div>
        </div>
    </div>
    <div style="clear:both"></div>
    <div style="clear:both;"></div>
</div>
