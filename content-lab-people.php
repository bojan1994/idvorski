<div class="left_section">
    <div class="osoblje" >
        <?php wp_title( '', true, '' ); ?>
    </div>
    <?php
    $args = array(
        'post_type' => 'lab_people',
    );
    $query = new WP_Query( $args );
    if( $query->have_posts() ) :
        while( $query->have_posts() ) :
            $query->the_post();
            ?>
            <div class="profil">
                <span class="purple_arrow"></span>
                <div class="profil_image">
           	    	<?php the_post_thumbnail( 'lab-people-image', array( 'class' => 'img-responsive', 'alt' => get_the_title() ) ); ?>
                </div>
                <div class="profil_description">
                	<?php echo get_post_meta( $post->ID, '_position_value_key', true ); ?><span> <?php the_title(); ?> <br /> <?php echo get_post_meta( $post->ID, '_education_value_key', true ); ?></span>
                    <?php the_content(); ?>
                </div>
                <div style="clear:both;"></div>
            </div>
            <?php
        endwhile;
        wp_reset_postdata();
    endif;
    ?>
</div>
