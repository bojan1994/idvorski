<div class="left_section">
    <div class="osoblje" style="font-family: Arial, Helvetica, sans-serif;">
        <?php wp_title( '', true, '' ); ?>
    </div>
    <?php
    if( is_page( 'Linkovi' ) || is_page( 'Links' ) ) {
        $args = array(
            'post_type' => 'links',
            'posts_per_page' => 50,
        );
    } elseif( is_page( 'Partneri' ) || is_page( 'Partners' ) ) {
        $args = array(
            'post_type' => 'partners',
            'posts_per_page' => 50,
        );
    }
    $query = new WP_Query( $args );
    if( $query->have_posts() ) :
        while( $query->have_posts() ) :
            $query->the_post();
            ?>
            <div class="links">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border:none">
                            <div class="links_image">
                                <?php
                                $post_thumbnail_id = get_post_thumbnail_id();
                                $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
                                ?>
                                <a href="<?php echo get_post_meta( $post->ID, '_links_value_key', true ); ?>" target="_blank" rel="no-follow"><img src="<?php echo $post_thumbnail_url; ?>" width="70" /></a>
                            </div>
                        </td>
                        <td style="border:none">
                            <div class="links_description">
                                <div class="text_links">
                                    <span style="font-family:Arial, Helvetica, sans-serif; font-size:17px" ><?php the_title(); ?></span>
                                    <a href="http://www.ats.rs" target="_blank" rel="no-follow"><?php the_content(); ?></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <div style="clear:both;"></div>
            </div>
            <?php
        endwhile;
        wp_reset_postdata();
    endif;
    ?>
</div>
