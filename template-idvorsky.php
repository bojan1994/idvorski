<?php

/* Template Name: O EMC, O nama, Usluge, Radio i TT oprema */

get_header(); ?>

<div id="content" style="position:relative;">
    <?php
    get_template_part( 'content', 'top-image' );
    get_template_part( 'content', 'page-body' );
    get_template_part( 'content', 'side-news' );
    ?>
</div>
<div style="clear:both;"></div>

<?php get_footer();
