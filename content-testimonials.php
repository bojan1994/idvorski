<?php

$args = array(
    'post_type' => 'testimonials',
    'posts_per_page' => 2,
);
$query = new WP_Query( $args );
if( $query->have_posts() ) :
    ?>
    <div style="width:1024px;">
        <div class="title">
            <a href="<?php echo get_permalink( get_page_by_title( 'Rekli su o nama' ) ); ?>">
            	<?php
                if( isset($_GET['lang']) && $_GET['lang'] == 'en' ) {
                    ?>
                    <img src="<?php bloginfo('template_url'); ?>/img/reference.en.jpg" height="41" />
                    <?php
                } else {
                    ?>
                    <img src="<?php bloginfo('template_url'); ?>/img/reference.rs.jpg" height="41" />
                    <?php
                }
                ?>
            </a>
        </div>
        <div class="text_section_blue" style="width:1024px;height:auto;padding:0px 0px 10px 0px;background-color:#11a8db">
            <div class="arrow_brown">
            	<a href="<?php echo get_permalink( get_page_by_title( 'Rekli su o nama' ) ); ?>" class="small_arrow"></a>
            </div>
            <div class="text_box_blue" style="width:960px;">
            <?php
            while( $query->have_posts() ) :
                $query->the_post();
                ?>
                <div>
                    <div style="float:left; padding:2px 0 0 0; margin:0 10px 0 0">
                    	<a href="<?php the_permalink(); ?>">
                    		<img src="<?php bloginfo('template_url'); ?>/img/quote_home.jpg" height="34"/>
                         </a>
                    </div>
                    <div class="small_box" style="margin:0 10px 15px 0px;">
                       	<div class="arrow_small">
                        	<a href="<?php the_permalink(); ?>" class="small_arrow">
                        	</a>
                        </div>
                        <div class="testimonials-box">
                            <div>
                                <?php the_title(); ?>, <?php echo get_post_meta( $post->ID, '_testimonials_value_key', true ); ?>
                            </div>
                            <a href="<?php the_permalink(); ?>" >
                                <?php the_excerpt(); ?>
                            </a>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                </div>
                <?php
            endwhile;
            wp_reset_postdata();
            ?>
            </div>
        </div>
    </div>
    <?php
else :
    _e( 'Sorry, no content found', 'Idvorsky' );
endif;
