<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

if(isset($_POST['btn_submit'])) {
    if(!empty($_POST['message']) && !empty($_POST['fullname']) && !empty($_POST['company']) && !empty($_POST['street_number']) && !empty($_POST['city']) && !empty($_POST['phone']) && !empty($_POST['fax']) && !empty($_POST['email']) && !empty($_POST['web_address'])) {
        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $message = strip_tags($_POST['message']);
            $fullname = strip_tags($_POST['fullname']);
            $company = strip_tags($_POST['company']);
            $number = strip_tags($_POST['street_number']);
            $city = strip_tags($_POST['city']);
            $phone = strip_tags($_POST['phone']);
            $fax = strip_tags($_POST['fax']);
            $email = strip_tags($_POST['email']);
            $address = strip_tags($_POST['web_address']);
            $mail = new PHPMailer(true);
            $mail->setFrom($email, $fullname);
            $mail->addAddress('bojan.mihajlovic@m1.rs','Bojan Mihajlovic');
            $mail->addReplyTo($email, $fullname);
            $mail->isHTML(true);
            $mail->Subject = 'Contact form message: ';
            $mail->Body   = 'Kompanija: ' . $company;
            $mail->Body   .= '<br>';
            $mail->Body   .= 'Ulica i broj: ' . $number;
            $mail->Body   .= '<br>';
            $mail->Body   .= 'Grad: ' . $city;
            $mail->Body   .= '<br>';
            $mail->Body   .= 'Kontakt telefon: ' . $phone;
            $mail->Body   .= '<br>';
            $mail->Body   .= 'Faks: ' . $fax;
            $mail->Body   .= '<br>';
            $mail->Body   .= 'Internet strana kompanije: ' . $address;
            $mail->Body   .= '<br>';
            $mail->Body   .= 'Poruka: ' . $message;
            $mail->AltBody = $message;
            $success = $mail->send();
            if($success) {
                echo '<script>alert("Your message has been sent.")</script>';
            } else {
                echo '<script>alert("There was an error sending this message.")</script>';
            }
        } else {
            echo '<script>alert("Invalid e-mail address.")</script>';
        }
    } else {
        echo '<script>alert("All fields are required.")</script>';
    }
}
