<?php
require __DIR__ . '../../../../wp-load.php';

$paged = isset($_GET['current']) ? intval($_GET['current']) : 1;
?>
    <span class="purple_arrow"></span>
    <table cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <th nowrap>Broj</th>
                    <th nowrap>Vrsta isprave</th>
                    <th nowrap>Vrsta opreme</th>
                    <th nowrap>Proizvođač</th>
                    <th nowrap>Oznaka tipa/model</th>
                    <th nowrap>Datum izdavanja</th>
                    <th nowrap>Važi do</th>
                    <th nowrap></th>
                </tr>
                <?php
                $page = isset($_GET['page']) ? $_GET['page'] : '' ;
                switch($page) {
                    case 277:
                        $args = array(
                            'post_type' => 'ritt',
                            'posts_per_page' => isset($_GET['per_page']) ? intval($_GET['per_page']) : 10,
                            'paged' => $paged,
                            'order' => isset($_GET['sort']) ? $_GET['sort'] : 'ASC',
                            'meta_key' => '_ritt_number_value_key',
                            'orderby' => 'meta_value',
                            'tax_query' => array(
                                'relation' => 'AND',
                                array(
                                    'taxonomy' => 'ritt_category',
                                    'field' => 'slug',
                                    'terms' => array( 'sertifikacija' ),
                                    'operator' => 'IN',
                                ),
                            ),
                        );
                        break;
                    case 402:
                        $args = array(
                            'post_type' => 'ritt',
                            'posts_per_page' => isset($_GET['per_page']) ? intval($_GET['per_page']) : 10,
                            'paged' => $paged,
                            'order' => isset($_GET['sort']) ? $_GET['sort'] : 'ASC',
                            'meta_key' => '_ritt_number_value_key',
                            'orderby' => 'meta_value',
                            'tax_query' => array(
                                'relation' => 'AND',
                                array(
                                    'taxonomy' => 'ritt_category',
                                    'field' => 'slug',
                                    'terms' => array( 'sertifikacija-emc' ),
                                    'operator' => 'IN',
                                ),
                            ),
                        );
                        break;
                    case 405:
                        $args = array(
                            'post_type' => 'ritt',
                            'posts_per_page' => isset($_GET['per_page']) ? intval($_GET['per_page']) : 10,
                            'paged' => $paged,
                            'order' => isset($_GET['sort']) ? $_GET['sort'] : 'ASC',
                            'meta_key' => '_ritt_number_value_key',
                            'orderby' => 'meta_value',
                            'tax_query' => array(
                                'relation' => 'AND',
                                array(
                                    'taxonomy' => 'ritt_category',
                                    'field' => 'slug',
                                    'terms' => array( 'sertifikacija-radio-i-tt-opreme' ),
                                    'operator' => 'IN',
                                ),
                            ),
                        );
                        break;
                    default:
                        $args = array(
                        );
                        break;
                }
                isset($_GET['s']) ? $args['s'] = $_GET['s'] : '';
                $query = new WP_Query( $args );
                if( $query->have_posts() ) :
                    while( $query->have_posts() ) :
                        $query->the_post();
                        ?>
                        <tr>
                            <td>
                                <?php echo get_post_meta( $post->ID, '_ritt_number_value_key', true ); ?>
                                <div class="more-info">
                                    Detaljnije:<?php the_content(); ?>
                                </div>
                            </td>
                            <td><?php echo get_post_meta( $post->ID, '_ritt_vrsta_isprave_value_key', true ); ?></td>
                            <td><?php echo get_post_meta( $post->ID, '_ritt_vrsta_opreme_value_key', true ); ?></td>
                            <td><?php the_title(); ?></td>
                            <td><?php echo get_post_meta( $post->ID, '_ritt_oznaka_tipa_value_key', true ); ?></td>
                            <td><?php echo get_post_meta( $post->ID, '_ritt_datum_izdavanja_value_key', true ); ?></td>
                            <td><?php echo get_post_meta( $post->ID, '_ritt_vazi_do_value_key', true ); ?></td>
                            <td><a href="#" class="more-info-button">Detaljnije</a></td>
                        </tr>
                        <?php
                    endwhile;
                    wp_reset_postdata();
                endif;
                ?>
            </tbody>
        </table>
        <div id="page-data" data-max="<?php echo $query->max_num_pages; ?>" data-current="<?php echo $paged; ?>"></div>
        <div class="pag-parent">
            <ul>
            <?php
                for ($i = 1; $i <= $query->max_num_pages; $i++) :
                    if ($i == $paged) :
                    ?>
                    <li class="page-numbers current"><?php echo $i; ?></li>
                    <?
                    else :
                    ?>
                    <li class="page-numbers"><a href="#<?php echo $i; ?>" onclick="changePage(<?php echo $i ?>)"><?php echo $i; ?></a></li>
                    <?
                    endif;
                endfor;
            ?>
            </ul>
        </div>
