<?php

/* Template Name: Ispitivanja */

get_header(); ?>

<div id="content" style="position:relative;">
    <?php
    get_template_part( 'content', 'top-image' );
    get_template_part( 'content', 'page-body' );
    get_template_part( 'content', 'documents' );
    ?>
</div>
<div style="clear:both;"></div>

<?php get_footer();
