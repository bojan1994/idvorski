<?php

if( isset($_GET['lang']) && $_GET['lang'] == 'en' ) {
    ?>
    <div class="right_section">
        <div class="box_news">
            <div class="title">
                <a href="<?php echo get_permalink( get_page_by_title( 'Vesti' ) ); ?>"><img src="<?php bloginfo('template_url'); ?>/img/uputstva.en.jpg" width="350" height="41" /></a>
            </div>
            <div class="text_section_news" id="text_section_news">
                <div class="arrow_brown">

                </div>
                <div class="text_box_blue">
                    <?php
                    if( is_page( 'Radio & TT Equipment Certification' ) ) {
                        ?>
                        <a href="<?php echo get_permalink( get_page( 402 ) ); ?>">
                            <div class="link_item">
                                <div class="link_document">

                                </div>
                                <div class="link_text">
                                    <?php
                                    if(isset($_GET['lang']) && $_GET['lang']) {
                                        ?>
                                        Registar of certified products R&TT
                                        <?php
                                    } else {
                                        ?>
                                        Evidencija izdatih isprava o RiTT
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div style="clear:left"></div>
                            </div>
                        </a>
                        <?php
                    } elseif( is_page( 'EMC Certification' ) ) {
                        ?>
                        <a href="<?php echo get_permalink( get_page( 405 ) ); ?>">
                            <div class="link_item">
                                <div class="link_document">

                                </div>
                                <div class="link_text">
                                    <?php
                                    if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
                                        ?>
                                        Registar of certified products
                                        <?php
                                    } else {
                                        ?>
                                        Evidencija izdatih isprava za EMC
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div style="clear:left"></div>
                            </div>
                        </a>
                        <?php
                    }
                    if( is_page( 'Certification' ) ) {
                        $args = array(
                            'post_type' => 'documents',
                            'tax_query' => array(
                                'relation' => 'AND',
                                array(
                                    'taxonomy' => 'documents_category',
                                    'field' => 'slug',
                                    'terms' => array( 'sertifikacija-en' ),
                                    'operator' => 'IN',
                                ),
                            ),
                        );
                    } elseif( is_page( 'Radio & TT Equipment Certification' ) ) {
                        $args = array(
                            'post_type' => 'documents',
                            'tax_query' => array(
                                'relation' => 'AND',
                                array(
                                    'taxonomy' => 'documents_category',
                                    'field' => 'slug',
                                    'terms' => array( 'sertifikacija-radio-i-tt-opreme-en' ),
                                    'operator' => 'IN',
                                ),
                            ),
                        );
                    } elseif( is_page( 'EMC Certification' ) ) {
                        $args = array(
                            'post_type' => 'documents',
                            'tax_query' => array(
                                'relation' => 'AND',
                                array(
                                    'taxonomy' => 'documents_category',
                                    'field' => 'slug',
                                    'terms' => array( 'sertifikacija-emc-en' ),
                                    'operator' => 'IN',
                                ),
                            ),
                        );
                    } elseif( is_page( 'Testing' ) ) {
                        $args = array(
                            'post_type' => 'documents',
                            'tax_query' => array(
                                'relation' => 'AND',
                                array(
                                    'taxonomy' => 'documents_category',
                                    'field' => 'slug',
                                    'terms' => array( 'ispitivanja-en' ),
                                    'operator' => 'IN',
                                ),
                            ),
                        );
                    }
                    $query = new WP_Query( $args );
                    if( $query->have_posts() ) :
                        while( $query->have_posts() ) :
                            $query->the_post();
                            $pdf = get_post_meta( $post->ID, '_pdf_value_key', true );
                            $document = get_post_meta( $post->ID, '_documents_value_key', true );
                            if( $pdf ) {
                                ?>
                                <a href="<?php echo $pdf['url']; ?>" target="_blank">
                                    <div class="link_item">
                                        <div class="link_document">
                                            <img src="<?php bloginfo('template_url'); ?>/img/pdf.png" style="width:25px;" />
                                        </div>
                                        <div class="link_text">
                                            <?php the_title(); ?>
                                        </div>
                                        <div style="clear:left"></div>
                                    </div>
                                </a>
                                <?php
                            } elseif( $document ) {
                                ?>
                                <a href="<?php echo $document; ?>" download="<?php echo $document; ?>">
                                    <div class="link_item">
                                        <div class="link_document">
                                            <img src="<?php bloginfo('template_url'); ?>/img/doc.png" style="width:25px;" />
                                        </div>
                                        <div class="link_text">
                                            <?php the_title(); ?>
                                        </div>
                                        <div style="clear:left"></div>
                                    </div>
                                </a>
                                <?php
                            }
                        endwhile;
                        wp_reset_postdata();
                    endif;
                    ?>
                </div>
                <div style="clear:both"></div>
            </div>
        </div>
    </div>
    <?php
} else {
    ?>
    <div class="right_section">
        <div class="box_news">
            <div class="title">
                <a href="<?php echo get_permalink( get_page_by_title( 'Vesti' ) ); ?>"><img src="<?php bloginfo('template_url'); ?>/img/uputstva.rs.jpg" width="350" height="41" /></a>
            </div>
            <div class="text_section_news" id="text_section_news">
                <div class="arrow_brown">

                </div>
                <div class="text_box_blue">
                    <?php
                    if( is_page( 'Sertifikacija radio i TT opreme' ) ) {
                        ?>
                        <a href="<?php echo get_permalink( get_page( 402 ) ); ?>">
                            <div class="link_item">
                                <div class="link_document">

                                </div>
                                <div class="link_text">
                                    Evidencija izdatih isprava o RiTT
                                </div>
                                <div style="clear:left"></div>
                            </div>
                        </a>
                        <?php
                    } elseif( is_page( 'Sertifikacija EMC' ) ) {
                        ?>
                        <a href="<?php echo get_permalink( get_page( 405 ) ); ?>">
                            <div class="link_item">
                                <div class="link_document">

                                </div>
                                <div class="link_text">
                                    Evidencija izdatih isprava za EMC
                                </div>
                                <div style="clear:left"></div>
                            </div>
                        </a>
                        <?php
                    }
                    if( is_page( 'Sertifikacija' ) ) {
                        $args = array(
                            'post_type' => 'documents',
                            'tax_query' => array(
                                'relation' => 'AND',
                                array(
                                    'taxonomy' => 'documents_category',
                                    'field' => 'slug',
                                    'terms' => array( 'sertifikacija' ),
                                    'operator' => 'IN',
                                ),
                            ),
                        );
                    } elseif( is_page( 'Sertifikacija radio i TT opreme' ) ) {
                        $args = array(
                            'post_type' => 'documents',
                            'tax_query' => array(
                                'relation' => 'AND',
                                array(
                                    'taxonomy' => 'documents_category',
                                    'field' => 'slug',
                                    'terms' => array( 'sertifikacija-radio-i-tt-opreme' ),
                                    'operator' => 'IN',
                                ),
                            ),
                        );
                    } elseif( is_page( 'Sertifikacija EMC' ) ) {
                        $args = array(
                            'post_type' => 'documents',
                            'tax_query' => array(
                                'relation' => 'AND',
                                array(
                                    'taxonomy' => 'documents_category',
                                    'field' => 'slug',
                                    'terms' => array( 'sertifikacija-emc' ),
                                    'operator' => 'IN',
                                ),
                            ),
                        );
                    } elseif( is_page( 'Ispitivanja' ) ) {
                        $args = array(
                            'post_type' => 'documents',
                            'tax_query' => array(
                                'relation' => 'AND',
                                array(
                                    'taxonomy' => 'documents_category',
                                    'field' => 'slug',
                                    'terms' => array( 'ispitivanja' ),
                                    'operator' => 'IN',
                                ),
                            ),
                        );
                    }
                    $query = new WP_Query( $args );
                    if( $query->have_posts() ) :
                        while( $query->have_posts() ) :
                            $query->the_post();
                            $pdf = get_post_meta( $post->ID, '_pdf_value_key', true );
                            $document = get_post_meta( $post->ID, '_documents_value_key', true );
                            if( $pdf ) {
                                ?>
                                <a href="<?php echo $pdf['url']; ?>" target="_blank">
                                    <div class="link_item">
                                        <div class="link_document">
                                            <img src="<?php bloginfo('template_url'); ?>/img/pdf.png" style="width:25px;" />
                                        </div>
                                        <div class="link_text">
                                            <?php the_title(); ?>
                                        </div>
                                        <div style="clear:left"></div>
                                    </div>
                                </a>
                                <?php
                            } elseif( $document ) {
                                ?>
                                <a href="<?php echo $document; ?>" download="<?php echo $document; ?>">
                                    <div class="link_item">
                                        <div class="link_document">
                                            <img src="<?php bloginfo('template_url'); ?>/img/doc.png" style="width:25px;" />
                                        </div>
                                        <div class="link_text">
                                            <?php the_title(); ?>
                                        </div>
                                        <div style="clear:left"></div>
                                    </div>
                                </a>
                                <?php
                            }
                        endwhile;
                        wp_reset_postdata();
                    endif;
                    ?>
                </div>
                <div style="clear:both"></div>
            </div>
        </div>
    </div>
    <?php
}
