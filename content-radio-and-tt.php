<?php

if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
    ?>
    <div class="box">
        <div class="title shedule">
            <a href="<?php echo get_theme_mod( 'radio_and_tt_text3_en' ); ?>">
                <img src="<?php bloginfo('template_url'); ?>/img/zakazivanje.en.png" alt="<?php echo get_theme_mod( 'radio_and_tt_text' ); ?>">
            </a>
        </div>
        <div class="text_section">
            <div class="arrow_brown">
                <a href="<?php echo get_theme_mod( 'radio_and_tt_text3_en' ); ?>" class="small_arrow"></a>
            </div>
            <div class="text_box radio-and-tt">
                <div style="height:59px;">
                    <a href="<?php echo get_theme_mod( 'radio_and_tt_text3_en' ); ?>">
                        <p><?php echo get_theme_mod( 'radio_and_tt_text2_en' ); ?></p>
                    </a>
                </div>
                <img src="<?php bloginfo('template_url'); ?>/img/home_ranfla.png" alt="Ranfla" style="margin:20px 0 0 4px;">
            </div>
        </div>
    </div>
    <?php
} else {
    ?>
    <div class="box">
        <div class="title shedule">
            <a href="<?php echo get_theme_mod( 'radio_and_tt_text3' ); ?>">
                <img src="<?php bloginfo('template_url'); ?>/img/zakazivanje.rs.png" alt="<?php echo get_theme_mod( 'radio_and_tt_text' ); ?>">
            </a>
        </div>
        <div class="text_section">
            <div class="arrow_brown">
                <a href="<?php echo get_theme_mod( 'radio_and_tt_text3' ); ?>" class="small_arrow"></a>
            </div>
            <div class="text_box radio-and-tt">
                <div style="height:59px;">
                    <a href="<?php echo get_theme_mod( 'radio_and_tt_text3' ); ?>">
                        <p><?php echo get_theme_mod( 'radio_and_tt_text2' ); ?></p>
                    </a>
                </div>
                <img src="<?php bloginfo('template_url'); ?>/img/home_ranfla.png" alt="Ranfla" style="margin:20px 0 0 4px;">
            </div>
        </div>
    </div>
    <?php
}
