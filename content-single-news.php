<div class="left_section">
    <div class="osoblje" >
        Vesti
    </div>
    <?php
    if( have_posts() ) :
        while( have_posts() ) :
            the_post();
            ?>
            <div style="color:#888;padding: 20px 20px 20px 20px;text-align:justify;position:relative; font-size:12px; line-height:18px;margin: 0 20px 0 0;" class="news_text">
            	<span class="purple_arrow"></span>
                <div class="news_name"><?php the_title(); ?></div>
                <div class="news_date_list">
                    <?php echo get_the_date( 'd.m.Y.' ); ?>
                </div>
        		<?php the_content(); ?>
            </div>
            <div style="padding:0px 0 20px 0;position:relative; border-bottom: 1px solid #bab8ab;margin: 0 20px 0 0;" class="text_news_desc">
                <div style="float:left">
                    <?php
                    $post_thumbnail_id = get_post_thumbnail_id();
                    $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
                    ?>
                    <a id="single_image" href="<?php echo $post_thumbnail_url; ?>">
                        <img src="<?php echo $post_thumbnail_url; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" style=" width: 290px;padding: 0 12px 13px 20px;" />
        			</a>
                </div>
               <div style="clear:both"></div>
            </div>
            <?php
        endwhile;
        wp_reset_postdata();
    endif;
    ?>
</div>
