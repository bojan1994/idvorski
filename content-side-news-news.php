<div class="right_section">
    <div class="box_news">
       <div class="title">
           <?php
           if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
               ?>
               <a href="<?php echo get_permalink( get_page_by_title( 'Vesti' ) ); ?>"><img src="<?php bloginfo('template_url'); ?>/img/vesti2.en.png" width="350" height="41" /></a>
               <?php
           } else {
               ?>
               <a href="<?php echo get_permalink( get_page_by_title( 'Vesti' ) ); ?>"><img src="<?php bloginfo('template_url'); ?>/img/vesti.rs.png" width="350" height="41" /></a>
               <?php
           }
           ?>
       </div>
       <div class="text_section_news" id="text_section_news">
            <div class="arrow_brown">
                  <a href="<?php echo get_permalink( get_page_by_title( 'Vesti' ) ); ?>" class="small_arrow"></a>
            </div>
            <div class="text_box_blue">
         		<?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 6,
                );
                $query = new WP_Query( $args );
                if( $query->have_posts() ) :
                    while( $query->have_posts() ) :
                        $query->the_post();
                        ?>
                        <div class="small_box" style="height:auto;padding:0px 0px 10px 0px">
            				<div class="arrow_small">
                            	<a href="<?php the_permalink(); ?>" class="small_arrow"></a>
                             </div>
                             <div style="font-size: 10px;">
                                 <?php echo get_the_date( 'd.m.Y.' ); ?>
                             </div>
                       	     <a href="<?php the_permalink(); ?>">
                                 <a href="<?php the_permalink(); ?>">
                                     <?php
                                     $post_thumbnail_id = get_post_thumbnail_id();
                                     $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
                                     ?>
                             	     <img src="<?php echo $post_thumbnail_url; ?>" style="float:left;padding:0px 10px 0px 0px; width:70px; height:47px" />
                                </a>
                                <div style="text-align:left; display:table-cell; vertical-align:middle; height:47px;width:160px;">
                                	<a href="<?php the_permalink(); ?>">
    						 			<?php the_title(); ?>
                                    </a>
                                </div>
                                <div style="clear:both"></div>
                             </a>
                         </div>
                        <?php
                    endwhile;
                    wp_reset_postdata();
                endif;
                ?>
             </div>
        </div>
    </div>
    <div style="clear:both"></div>
