<?php

/* Template name: Pitajte nas */

get_header(); ?>

<div id="content" style="position:relative;">
    <?php
    get_template_part( 'content', 'contact-form' );
    get_template_part( 'content', 'side-informations' );
    ?>
</div>
<div style="clear:both;"></div>

<?php get_footer();
