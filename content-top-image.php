<div class="top_content">
    <img src="<?php bloginfo('template_url'); ?>/img/senka_header_desna.png" style="position:absolute; top:0; right:-10px;" alt="Idvorsky laboratorije" />
    <img src="<?php bloginfo('template_url'); ?>/img/senka_header_leva.png" style="position:absolute; top:0; left:-10px;" alt="Idvorsky laboratorije" />
    <?php
    if( is_page( 'O EMC' ) || is_page( 'About EMC' ) ) {
        if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/o_emc.en.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        } else {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/o_emc.rs.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        }
    } elseif( is_page( 'O nama' ) || is_page( 'About Us' ) ) {
        if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/o_nama.en.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        } else {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/o_nama.rs.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        }
    } elseif( is_page( 'Osoblje laboratorije' ) || is_page( 'Staff' ) ) {
        if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/osoblje.en.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        } else {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/osoblje.rs.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        }
    } elseif( is_page( 'Ispitivanja' ) || is_page( 'Testing' ) ) {
        if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/Testings.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        } else {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/ispitivanja.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        }
    } elseif( is_page( 'Linkovi' ) || is_page( 'Links' ) ) {
        if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/linkovi.en.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        } else {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/linkovi.rs.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        }
    } elseif( is_page( 'Partneri' ) || is_page( 'Partners' ) ) {
        if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/headers-parners.en.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        } else {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/headers-parners.rs.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        }
    } elseif( is_page( 'Kontakt informacije' ) || is_page( 'Contact Info' ) ) {
        if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/contact_en.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        } else {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/contact_rs.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        }
    } elseif( is_page( 'Rekli su o nama' ) || is_page( 'Testimonials' ) ) {
        if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/header.en.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        } else {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/header.rs.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        }
    } elseif( is_singular( 'testimonials' ) ) {
        if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/header.en.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        } else {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/header.rs.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        }
    } elseif( is_page( 'Vesti' ) || is_page( 'News' ) ) {
        if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/vesti.en.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        } else {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/vesti.rs.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        }
    } elseif( is_singular( 'post' ) ) {
        ?>
        <img src="<?php bloginfo('template_url'); ?>/img/vesti.rs.jpg" width="1024" height="130" alt="Rekli su o nama" />
        <?php
    } elseif( is_tax( 'gallery_category' ) ) {
        if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/galerija.en.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        } else {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/galerija.rs.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        }
    } elseif( is_page( 'Sertifikacija' ) || is_page( 'Certification' ) ) {
        if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/Certification.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        } else {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/sertifikacija.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        }
    } elseif( is_page( 'Sertifikacija EMC' ) || is_page( 'EMC Certification' ) ) {
        if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/EMC.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        } else {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/EMC.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        }
    } elseif( is_page( 'Sertifikacija radio i TT opreme' ) || is_page( 'Radio & TT Equipment Certification' )  ) {
        if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/tt_oprema.en.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        } else {
            ?>
            <img src="<?php bloginfo('template_url'); ?>/img/tt_oprema.rs.jpg" width="1024" height="130" alt="O EMC" />
            <?php
        }
    } elseif( is_page( 'Evidencija izdatih isprava o RiTT' ) ) {
        ?>
        <img src="<?php bloginfo('template_url'); ?>/img/evidencijarit.jpg" width="1024" height="130" alt="Evidencija izdatih isprava o RiTT" />
        <?php
    } elseif( is_page( 'Evidencija izdatih isprava za EMC' ) ) {
        ?>
        <img src="<?php bloginfo('template_url'); ?>/img/evidencijaemc.jpg" width="1024" height="130" alt="Evidencija izdatih isprava za EMC" />
        <?php
    }
    ?>
</div>
