<div class="left_section">
<?php
if( have_posts() ) :
    while( have_posts() ) :
        the_post();
        ?>
            <div class="osoblje" >
            	<?php the_title(); ?>
            </div>
            <div class="text_about" >
                <span class="purple_arrow"></span>
                <?php the_content(); ?>
            </div>
        <?php
    endwhile;
endif;
?>
</div>
