<div class="right_section">
    <div class="box_news">
        <div class="title">
            <?php
            if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
                ?>
                <a href="javascript:void(0)"><img src="<?php bloginfo('template_url'); ?>/img/galerija.en.png" width="350" height="41" /></a>
                <?php
            } else {
                ?>
                <a href="javascript:void(0)"><img src="<?php bloginfo('template_url'); ?>/img/galerija.rs.png" width="350" height="41" /></a>
                <?php
            }
            ?>
        </div>
        <div class="gallery_sidebar" id="text_section_news">
        	<div class="arrow_brown">
            	<a href="javascript:void(0)" class="small_arrow"></a>
            </div><!--arrow_brown-->
        	<div class="text_box_blue">
                <?php
                $terms = get_terms( array(
                    'taxonomy' => 'gallery_category',
                    'hide_empty' => false,
                ) );
                foreach($terms as $term) {
                    ?>
                    <div class="small_box" style="height:auto;padding:0px 0px 10px 0px">
                        <div class="arrow_small">
                            <a href="javascript:void(0)" class="small_arrow"></a>
                        </div>
                        <a href="<?php echo get_term_link( $term ); ?>" style="text-decoration:none; color:#fff">
                            <?php echo $term->name; ?>
                        </a>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>
<div style="clear:both;"></div>

<div style="clear:both;"></div>
