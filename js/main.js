var $ = jQuery.noConflict();
$(function() {
/* Apply fancybox to multiple items */
$("a.group_images").fancybox();


$(".video").click(function() {
		$.fancybox({
			'padding'		: 0,
			'autoScale'		: false,
			'transitionIn'	: 'none',
			'transitionOut'	: 'none',
			'title'			: this.title,
			'width'			: 640,
			'height'		: 385,
			'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
			'type'			: 'swf',
			'swf'			: {
			'wmode'				: 'transparent',
			'allowfullscreen'	: 'true'
			}
		});

		return false;
	});

});

// $(function(){
// 	get_pictures(0);
// })
//
// function get_pictures(gallery_id){
// 	var language=$('#jezik').val();
// 	var call_data = 'gallery_id='+gallery_id+'&language='+language;
// 	var call_url = language+'/ajax_gallery';
// 	var callback = function(odgovor){
//         $('.left_section').html(odgovor);
//         $("a.group_images").fancybox();
// 	}
// 	ajax_call(call_url, call_data, callback);
// }

var current_first=1;
var current_second=2;
var size_of_news=6;
function change_news(){
	var next_first=current_second+1;
	if(next_first>6 || next_first>size_of_news){
		next_first=1;
	}
	var next_second=next_first+1;
	if(next_second>6 || next_second>size_of_news){
		next_second=1;
	}
	$('#news_'+current_first).fadeOut(300);
	$('#news_'+current_second).fadeOut(300,function(){
		$('#news_'+next_first).fadeIn(300);
		$('#news_'+next_second).fadeIn(300);
		current_first=next_first;
		current_second=next_second;
	});
    setTimeout("change_news()",6000);
}

$(function(){
	  setTimeout("change_news()",6000);
});

var ritt = function(s, perPage, sort, page, currentPage){
    $.ajax({
        type: "GET",
        url: $('body').data('baseurlforajax') + "/ritt-search.php",
        data: {
            "s": s,
            "per_page": perPage,
            "sort": sort,
            "page": page,
			"current": currentPage
        }
    }).done(function( data ) {
        $('#ritt_search').html(data);
		$('.more-info-button').click(function(e) {
			$(this).closest('tr').find($('.more-info')).toggle();
		});
		$('.pagination_item').click(function() {
			$(this).addClass('current');
			$('.pagination_item').not($(this)).removeClass('current');
		});
    });
}

$(function(){
    ritt();
});

var currentPage = 1;

function change_number_per_page(e){
	currentPage = window.location.hash ?parseInt(window.location.hash.substr(1)) : 1;
    var s = document.querySelectorAll('[name="search_field_search"]')[0].value;
    var perPage = e.value;
    var sort = document.querySelectorAll('[name="search_field_sort"]')[0].value;
    var page = document.querySelectorAll('[name="page"]')[0].value;
    ritt(s, perPage, sort, page, currentPage);
}

function set_search_string(e){
	currentPage = window.location.hash ?parseInt(window.location.hash.substr(1)) : 1;
    var s = e.value;
    var perPage = document.querySelectorAll('[name="search_field_per_page"]')[0].value;
    var sort = document.querySelectorAll('[name="search_field_sort"]')[0].value;
    var page = document.querySelectorAll('[name="page"]')[0].value;
    ritt(s, perPage, sort, page, currentPage);
}

function search_field_sort_change(e){
	currentPage = window.location.hash ?parseInt(window.location.hash.substr(1)) : 1;
    var s = document.querySelectorAll('[name="search_field_search"]')[0].value;
    var perPage = document.querySelectorAll('[name="search_field_per_page"]')[0].value;
    var sort = e.value;
    var page = document.querySelectorAll('[name="page"]')[0].value;
    ritt(s, perPage, sort, page, currentPage);
}

function search_field_page_load() {
	currentPage = window.location.hash ?parseInt(window.location.hash.substr(1)) : 1;
    var s = document.querySelectorAll('[name="search_field_search"]')[0].value;
    var perPage = document.querySelectorAll('[name="search_field_per_page"]')[0].value;
    var sort = document.querySelectorAll('[name="search_field_sort"]')[0].value;
    var page = document.querySelectorAll('[name="page"]')[0].value;
    ritt(s, perPage, sort, page, 1);
}

function changePage(cpage){
    var s = document.querySelectorAll('[name="search_field_search"]')[0].value;
    var perPage = document.querySelectorAll('[name="search_field_per_page"]')[0].value;
    var sort = document.querySelectorAll('[name="search_field_sort"]')[0].value;
    var page = document.querySelectorAll('[name="page"]')[0].value;
    ritt(s, perPage, sort, page, cpage);
}
