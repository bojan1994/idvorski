// JavaScript Document
function ajax_call(call_url,call_data,callback){
	$.ajax({
	  type: "POST",
	  url: call_url,
	  data:call_data,
	  cache: false,
	  success: callback,
	  error:function(jqXHR){
		if (jqXHR.status === 0) {
			alert('URL Address is not with www parameter.');
		} else if (jqXHR.status == 403) {
			alert('PHP File is not implemented in .htaccess file. [403]');
		} else if (jqXHR.status == 500) {
			alert('Internal Server Error [500].');
		} else if (exception === 'parsererror') {
			alert('Requested JSON parse failed.');
		} else if (exception === 'timeout') {
			alert('Time out error.');
		} else if (exception === 'abort') {
			alert('Ajax request aborted.');
		} else {
			alert('Uncaught Error.\n' + jqXHR.responseText);
		}
	  }
	});
}

function ajax_json_call(call_url,call_data,callback){
	$.ajax({
	  type: "POST",
	  dataType: "json",
	  url: call_url,
	  data:call_data,
	  cache: false,
	  success: callback,
	  error:function(jqXHR){
		if (jqXHR.status === 0) {
			alert('URL Address is not with www parameter.');
		} else if (jqXHR.status == 403) {
			alert('PHP File is not implemented in .htaccess file. [403]');
		} else if (jqXHR.status == 404) {
			alert('Requested page not found. [404]');
		} else if (jqXHR.status == 500) {
			alert('Internal Server Error [500].');
		} else if (exception === 'parsererror') {
			alert('Requested JSON parse failed.');
		} else if (exception === 'timeout') {
			alert('Time out error.');
		} else if (exception === 'abort') {
			alert('Ajax request aborted.');
		} else {
			alert('Uncaught Error.\n' + jqXHR.responseText);
		}
	  }
	});
}

function ajax_json_call_callback_example(json_responce){
	if(json_responce.success == 1){
		alert(json_responce.message);
	}else{
		alert(json_responce.message);
	}
}
