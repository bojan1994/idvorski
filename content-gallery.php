<?php

if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
    ?>
    <div class="left_section" style="width:674px;height:auto;">
        <div class="osoblje">
            <?php
            $taxonomy = get_queried_object();
            echo $taxonomy->name;
            ?>
        </div>
        <?php
        if( is_tax( 'gallery_category','demos' ) ) {
            $args = array(
                'post_type' => 'gallery',
                'tag' => 'video-en',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'gallery_category',
                        'field' => 'slug',
                        'terms' => 'demos',
                        'operator' => 'IN'
                    )
                ),
            );
        } elseif( is_tax( 'gallery_category','laboratory' ) ) {
            $args = array(
                'post_type' => 'gallery',
                'tag' => 'video-en',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'gallery_category',
                        'field' => 'slug',
                        'terms' => 'laboratory',
                        'operator' => 'IN'
                    )
                ),
            );
        }
        if( is_tax( 'gallery_category','demos' ) || is_tax( 'gallery_category','laboratory' ) ) {
            ?>
            <div id="gallery_content">
                <span class="purple_arrow"></span>
                <?php
                $query = new WP_Query( $args );
                if( $query->have_posts() ) :
                    while( $query->have_posts() ) :
                        $query->the_post();
                        ?>
                        <div class="gallery_item">
                            <a class="video iframe" rel="group1" href="<?php echo get_post_meta( $post->ID, '_videos_value_key', true ); ?>" title="<?php the_title(); ?>">
                                <div style="position:relative;height:91px;">
                                    <img src="<?php bloginfo('template_url'); ?>/img/7145_Untitled.png" alt="<?php the_title(); ?>" width="136" height="91" style="position:absolute">
                                    <img src="<?php bloginfo('template_url'); ?>/img/video.png" alt="<?php the_title(); ?>" width="136" height="91" style="position:absolute">
                                </div>
                            </a>
                            <div class="description">
                                <?php the_title(); ?>
                            </div>
                        </div>
                        <?php
                    endwhile;
                    wp_reset_postdata();
                endif;
                ?>
            </div>
                <?php
            }
            ?>
        <div style="clear:both"></div>
            <?php
            if( is_tax( 'gallery_category','laboratory' ) ) {
                $args2 = array(
                    'post_type' => 'gallery',
                    'tag' => 'photo-en',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'gallery_category',
                            'field' => 'slug',
                            'terms' => 'laboratory',
                            'operator' => 'IN'
                        )
                    ),
                );
            } elseif( is_tax( 'gallery_category','team' ) ) {
                $args2 = array(
                    'post_type' => 'gallery',
                    'tag' => 'photo-en',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'gallery_category',
                            'field' => 'slug',
                            'terms' => 'team',
                            'operator' => 'IN'
                        )
                    ),
                );
            }
            if( is_tax( 'gallery_category','laboratory' ) || is_tax( 'gallery_category','team' ) ) {
                ?>
                <div id="gallery_content">
                    <span class="purple_arrow"></span>
                    <?php
                    $query2 = new WP_Query( $args2 );
                    if( $query2->have_posts() ) :
                        while( $query2->have_posts() ) :
                            $query2->the_post();
                            ?>
                            <div class="gallery_item">
                                <?php
                                $post_thumbnail_id = get_post_thumbnail_id();
                                $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
                                ?>
                                <a class="group_images" rel="group1" href="<?php echo $post_thumbnail_url; ?>">
                                    <img src="<?php echo $post_thumbnail_url; ?>" alt="<?php the_title(); ?>" width="136" height="91">
                                </a>
                                <div class="description"></div>
                            </div>
                            <?php
                        endwhile;
                        wp_reset_postdata();
                    endif;
                    ?>
                </div>
                <?php
            }
            ?>
        <div style="clear:both"></div>
    </div>
    <?php
} else {
    ?>
    <div class="left_section" style="width:674px;height:auto;">
        <div class="osoblje">
            <?php
            $taxonomy = get_queried_object();
            echo $taxonomy->name;
            ?>
        </div>
        <?php
        if( is_tax( 'gallery_category','demonstracije' ) ) {
            $args = array(
                'post_type' => 'gallery',
                'tag' => 'video',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'gallery_category',
                        'field' => 'slug',
                        'terms' => 'demonstracije',
                        'operator' => 'IN'
                    )
                ),
            );
        } elseif( is_tax( 'gallery_category','laboratorija' ) ) {
            $args = array(
                'post_type' => 'gallery',
                'tag' => 'video',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'gallery_category',
                        'field' => 'slug',
                        'terms' => 'laboratorija',
                        'operator' => 'IN'
                    )
                ),
            );
        }
        if( is_tax( 'gallery_category','demonstracije' ) || is_tax( 'gallery_category','laboratorija' ) ) {
            ?>
            <div id="gallery_content">
                <span class="purple_arrow"></span>
                <?php
                $query = new WP_Query( $args );
                if( $query->have_posts() ) :
                    while( $query->have_posts() ) :
                        $query->the_post();
                        ?>
                        <div class="gallery_item">
                            <a class="video iframe" rel="group1" href="<?php echo get_post_meta( $post->ID, '_videos_value_key', true ); ?>" title="<?php the_title(); ?>">
                                <div style="position:relative;height:91px;">
                                    <img src="<?php bloginfo('template_url'); ?>/img/7145_Untitled.png" alt="<?php the_title(); ?>" width="136" height="91" style="position:absolute">
                                    <img src="<?php bloginfo('template_url'); ?>/img/video.png" alt="<?php the_title(); ?>" width="136" height="91" style="position:absolute">
                                </div>
                            </a>
                            <div class="description">
                                <?php the_title(); ?>
                            </div>
                        </div>
                        <?php
                    endwhile;
                    wp_reset_postdata();
                endif;
                ?>
            </div>
                <?php
            }
            ?>
        <div style="clear:both"></div>
            <?php
            if( is_tax( 'gallery_category','laboratorija' ) ) {
                $args2 = array(
                    'post_type' => 'gallery',
                    'tag' => 'photo',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'gallery_category',
                            'field' => 'slug',
                            'terms' => 'laboratorija',
                            'operator' => 'IN'
                        )
                    ),
                );
            } elseif( is_tax( 'gallery_category','tim' ) ) {
                $args2 = array(
                    'post_type' => 'gallery',
                    'tag' => 'photo',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'gallery_category',
                            'field' => 'slug',
                            'terms' => 'tim',
                            'operator' => 'IN'
                        )
                    ),
                );
            }
            if( is_tax( 'gallery_category','laboratorija' ) || is_tax( 'gallery_category','tim' ) ) {
                ?>
                <div id="gallery_content">
                    <span class="purple_arrow"></span>
                    <?php
                    $query2 = new WP_Query( $args2 );
                    if( $query2->have_posts() ) :
                        while( $query2->have_posts() ) :
                            $query2->the_post();
                            ?>
                            <div class="gallery_item">
                                <?php
                                $post_thumbnail_id = get_post_thumbnail_id();
                                $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
                                ?>
                                <a class="group_images" rel="group1" href="<?php echo $post_thumbnail_url; ?>">
                                    <img src="<?php echo $post_thumbnail_url; ?>" alt="<?php the_title(); ?>" width="136" height="91">
                                </a>
                                <div class="description"></div>
                            </div>
                            <?php
                        endwhile;
                        wp_reset_postdata();
                    endif;
                    ?>
                </div>
                <?php
            }
            ?>
        <div style="clear:both"></div>
    </div>
    <?php
}
