<div class="right_section" style="margin:0;">
    <div class="white_arrow2"></div>
    <div class="idvorski_box">
        <div class="labs">
              <img src="<?php bloginfo('template_url'); ?>/img/idvorskylab.jpg" alt="Idvorsky" />
          </div>
        <div class="information">
            <?php
            echo nl2br( get_post_meta( get_the_ID(), '_informations_value_key', true ) );
            ?>
        </div>
    </div>
</div>
