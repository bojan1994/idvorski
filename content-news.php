<?php

$args = array(
    'post_type' => 'post',
    'posts_per_page' => 6,
);
$query = new WP_Query( $args );
if( $query->have_posts() ) :
    ?>
    <div class="box_last">
        <div class="title news">
            <?php
            if(isset($_GET['lang']) && $_GET['lang'] == 'en' ) {
                ?>
                <a href="<?php echo get_permalink( get_page_by_title( 'Vesti' ) ); ?>"><img src="<?php bloginfo('template_url'); ?>/img/vesti2.en.png" width="350" height="41" /></a>
                <?php
            } else {
                ?>
                <a href="<?php echo get_permalink( get_page_by_title( 'Vesti' ) ); ?>"><img src="<?php bloginfo('template_url'); ?>/img/vesti.rs.png" width="350" height="41" /></a>
                <?php
            }
            ?>
        </div>
        <div class="text_section_blue">
            <div class="arrow_brown">
                <a href="<?php echo get_permalink( get_page_by_title( 'Vesti' ) ); ?>" class="small_arrow"></a>
            </div>
            <div class="text_box_blue">
            <?php
            $count = 0;
            while( $query->have_posts() ) :
                $query->the_post();
                $count++;
                ?>
                <div id="news_<?php echo $count; ?>" <?php echo ($count == 1 || $count == 2 ) ? '' : 'style="display: none;"'; ?>>
                    <div style="float:left; padding:2px 0 0 0; margin:0 10px 0 0">
                        <a href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail( 'news-image', array( 'class' => 'img-responsive', 'alt' => get_the_title() ) ); ?>
                        </a>
                    </div>
                    <div class="small_box" style="margin:0 10px 15px 59px;">
                        <div class="arrow_small">
                            <a href="<?php the_permalink(); ?>" class="small_arrow"></a>
                        </div>
                        <div>
                            <div style="font-size:10px;">
                                <?php echo get_the_date( 'd.m.Y.' ); ?>
                            </div>
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                    </div>
                </div>
            <?php
            endwhile;
            wp_reset_postdata();
            ?>
            </div>
        </div>
    </div>
    <div style="clear:both;"></div>
    <div style="height:10px;"></div>
    <?php
else :
    _e( 'Sorry, no content found', 'Idvorsky' );
endif;
