<?php

/* Template name: RiTT */

get_header(); ?>

<div id="content" style="position:relative;">
    <?php
    get_template_part( 'content', 'top-image' );
    get_template_part( 'content', 'ritt' );
    ?>
</div>
<div style="clear:both;"></div>

<?php get_footer();
