<div class="left_section">
    <div id="map" style="position: relative; overflow: hidden; width:674px; height:502px;"></div>
    <script>
        function initMap(){
            var options = {
                zoom:15,
                center:{lat:<?php echo get_post_meta( $post->ID, '_latitude_value_key', true ); ?> ,lng:<?php echo get_post_meta( $post->ID, '_longitude_value_key', true ); ?>}
            };
            var map = new google.maps.Map(document.getElementById('map'),options);
            var marker = new google.maps.Marker({
                position:{lat:<?php echo get_post_meta( $post->ID, '_latitude_value_key', true ); ?> ,lng:<?php echo get_post_meta( $post->ID, '_longitude_value_key', true ); ?>},
                map:map
            });
            var infoWindow = new google.maps.InfoWindow({
                content:'<h3>Idvorsky Laboratorije</h3>'
            });
            marker.addListener('click',function(){
                infoWindow.open(map,marker);
            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDBGwFyGCyj-ZokTR20y_pZ6ng-AtI-7E4&callback=initMap"
    async defer></script>
</div>
