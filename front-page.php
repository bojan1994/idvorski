<?php

get_header(); ?>

<div id="content" style="position:relative;">
    <?php
    get_template_part( 'content', 'slider' );
    get_template_part( 'content', 'ask-us' );
    get_template_part( 'content', 'radio-and-tt');
    get_template_part( 'content', 'news' );
    get_template_part( 'content', 'testimonials' );
    ?>
</div>
<div style="clear:both;"></div>

<?php get_footer();
