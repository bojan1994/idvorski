            <div class="clear:both;"></div>

            <div id="footer">
                <?php
                if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
                    echo get_theme_mod( 'custom_footer_text_en' ); 
                } else {
                    echo get_theme_mod( 'custom_footer_text' );
                }
                ?>
            </div>

        </div>

        <?php wp_footer(); ?>

    </body>

</html>
