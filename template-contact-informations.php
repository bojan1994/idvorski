<?php

/* Template name: Kontakt informacije */

get_header(); ?>

<div id="content" style="position:relative;">
    <?php
    get_template_part( 'content', 'top-image' );
    get_template_part( 'content', 'map' );
    get_template_part( 'content', 'side-informations' );
    ?>
</div>
<div style="clear:both;"></div>

<?php get_footer();
