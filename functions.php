<?php

/* Register scripts */
if( ! function_exists( 'register_scripts' ) ) {
    function register_scripts() {
        wp_enqueue_style( 'fancyboxcss', get_template_directory_uri() . '/css/jquery.fancybox-1.3.4.css' );
        wp_enqueue_style( 'style', get_stylesheet_uri() );
        wp_enqueue_script( 'ajax-call', get_template_directory_uri() . '/js/ajax_call.js', array( 'jquery' ) );
        wp_enqueue_script( 'mousewheel', get_template_directory_uri() . '/js/jquery.mousewheel-3.0.4.pack.js', array( 'ajax-call' ), false, false );
        wp_enqueue_script( 'fancybox1.3.4', get_template_directory_uri() . '/js/jquery.fancybox-1.3.4.pack.js', array( 'mousewheel' ), false, false );
        //wp_enqueue_script( 'fancyboxmedia', get_template_directory_uri() . '/js/jquery.fancybox-media.js', array( 'fancybox1.3.4' ), false, false );
        wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array( 'ajax-call' ), false, false );
    }
add_action( 'wp_enqueue_scripts', 'register_scripts' );
}

/* Theme Setup */
if( ! function_exists( 'theme_setup' ) ) {
    function theme_setup() {
        $array = array(
            'height'      => 65,
            'width'       => 450,
            'flex-height' => true,
            'flex-width'  => true,
            'header-text' => array( 'site-title', 'site-description' ),
        );
        add_theme_support( 'custom-logo', $array );
        add_image_size( 'news-image', 40, 40, true );
        add_image_size( 'news-image-medium', 70, 50, true );
        add_image_size( 'lab-people-image', 200, 150, true );
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'title-tag' );
    }
    add_action( 'after_setup_theme', 'theme_setup' );
}

/* Custom footer */
if( ! function_exists( 'theme_customizer' ) ) {
    function theme_customizer( $wp_customize ) {
        $wp_customize->add_setting( 'custom_footer_text',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'footer_text',array(
            'title' => __( 'Custom footer text','Idvorsky' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'custom_footer',array(
            'label' => __( 'Footer Text','Idvorsky' ),
            'section' => 'footer_text',
            'settings' => 'custom_footer_text',
        ) ) );

        $wp_customize->add_setting( 'custom_footer_text_en',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'footer_text_en',array(
            'title' => __( 'Custom footer text EN','Idvorsky' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'custom_footer_en',array(
            'label' => __( 'Footer Text','Idvorsky' ),
            'section' => 'footer_text_en',
            'settings' => 'custom_footer_text_en',
        ) ) );
    }
    add_action( 'customize_register','theme_customizer' );
}

/* Ask us footer */
if( ! function_exists( 'theme_customizer2' ) ) {
    function theme_customizer2( $wp_customize ) {
        $wp_customize->add_section( 'ask_text',array(
            'title' => __( 'Pitajte nas','Idvorsky' ),
            'priority' => 160,
        ) );
        $wp_customize->add_setting( 'ask_us_text',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_setting( 'ask_us_text2',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_setting( 'ask_us_text3',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'ask_us',array(
            'label' => __( 'Title','Idvorsky' ),
            'section' => 'ask_text',
            'settings' => 'ask_us_text',
        ) ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'ask_us2',array(
            'label' => __( 'Content','Idvorsky' ),
            'section' => 'ask_text',
            'settings' => 'ask_us_text2',
            'type' => 'textarea'
        ) ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'ask_us3',array(
            'label' => __( 'Link','Idvorsky' ),
            'section' => 'ask_text',
            'settings' => 'ask_us_text3',
        ) ) );
        $wp_customize->add_section( 'ask_text_en',array(
            'title' => __( 'Pitajte nas EN','Idvorsky' ),
            'priority' => 160,
        ) );
        $wp_customize->add_setting( 'ask_us_text_en',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_setting( 'ask_us_text2_en',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_setting( 'ask_us_text3_en',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'ask_us_en',array(
            'label' => __( 'Title','Idvorsky' ),
            'section' => 'ask_text_en',
            'settings' => 'ask_us_text_en',
        ) ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'ask_us2_en',array(
            'label' => __( 'Content','Idvorsky' ),
            'section' => 'ask_text_en',
            'settings' => 'ask_us_text2_en',
            'type' => 'textarea'
        ) ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'ask_us3_en',array(
            'label' => __( 'Link','Idvorsky' ),
            'section' => 'ask_text_en',
            'settings' => 'ask_us_text3_en',
        ) ) );
    }
    add_action( 'customize_register','theme_customizer2' );
}

/* Radio and TT */
if( ! function_exists( 'theme_customizer3' ) ) {
    function theme_customizer3( $wp_customize ) {
        $wp_customize->add_section( 'radio_and_tt',array(
            'title' => __( 'Radio i TT oprema','Idvorsky' ),
            'priority' => 160,
        ) );
        $wp_customize->add_setting( 'radio_and_tt_text',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_setting( 'radio_and_tt_text2',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_setting( 'radio_and_tt_text3',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'radio_tt',array(
            'label' => __( 'Title','Idvorsky' ),
            'section' => 'radio_and_tt',
            'settings' => 'radio_and_tt_text',
        ) ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'radio_tt2',array(
            'label' => __( 'Content','Idvorsky' ),
            'section' => 'radio_and_tt',
            'settings' => 'radio_and_tt_text2',
            'type' => 'textarea'
        ) ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'radio_tt3',array(
            'label' => __( 'Link','Idvorsky' ),
            'section' => 'radio_and_tt',
            'settings' => 'radio_and_tt_text3',
        ) ) );
        $wp_customize->add_section( 'radio_and_tt_en',array(
            'title' => __( 'Radio i TT oprema EN','Idvorsky' ),
            'priority' => 160,
        ) );
        $wp_customize->add_setting( 'radio_and_tt_text_en',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_setting( 'radio_and_tt_text2_en',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_setting( 'radio_and_tt_text3_en',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'radio_tt_en',array(
            'label' => __( 'Title','Idvorsky' ),
            'section' => 'radio_and_tt_en',
            'settings' => 'radio_and_tt_text_en',
        ) ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'radio_tt2_en',array(
            'label' => __( 'Content','Idvorsky' ),
            'section' => 'radio_and_tt_en',
            'settings' => 'radio_and_tt_text2_en',
            'type' => 'textarea'
        ) ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'radio_tt3_en',array(
            'label' => __( 'Link','Idvorsky' ),
            'section' => 'radio_and_tt_en',
            'settings' => 'radio_and_tt_text3_en',
        ) ) );
    }
    add_action( 'customize_register','theme_customizer3' );
}

/* Logo en */
if( ! function_exists( 'theme_customizer4' ) ) {
    function theme_customizer4( $wp_customize ) {
        $wp_customize->add_setting( 'logo_en',array(
           'default' => '',
           'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'logo',array(
           'title' => __( 'Logo EN','beogroup' ),
           'priority' => 160,
        ) );
        $wp_customize->add_control(
            new WP_Customize_Image_Control(
                $wp_customize,
                'en',
                array(
                    'label'      => __( 'Upload logo', 'bendigo-bank' ),
                    'section'    => 'logo',
                    'settings'   => 'logo_en',
                )
            )
        );
    }
    add_action( 'customize_register','theme_customizer4' );
}

/* Register Navigation Menus */
register_nav_menus( array(
    'primary' => __( 'Primary Navigation' ),
    'languages' => __( 'Languages' ),
) );

/* Testimonials post type */
if( ! function_exists( 'testimonials_post_type' ) ) {
    function testimonials_post_type() {
        $labels = array(
            'name' => __( 'Rekli su o nama', 'Idvorsky' ),
            'singular_name' => __('Rekli su o nama', 'Idvorsky'),
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'testimonials',
            ),
        );
        register_post_type( 'testimonials', $args );
    }
    add_action( 'init', 'testimonials_post_type' );
}

/* Lab people post type */
if( ! function_exists( 'lab_people_post_type' ) ) {
    function lab_people_post_type() {
        $labels = array(
            'name' => __( 'Osoblje laboratorije', 'Idvorsky' ),
            'singular_name' => __('Osoblje laboratorije', 'Idvorsky'),
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'lab_people',
            ),
        );
        register_post_type( 'lab_people', $args );
    }
    add_action( 'init', 'lab_people_post_type' );
}

/* Links post type */
if( ! function_exists( 'links_post_type' ) ) {
    function links_post_type() {
        $labels = array(
            'name' => __( 'Linkovi', 'Idvorsky' ),
            'singular_name' => __('Linkovi', 'Idvorsky'),
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'links',
            ),
        );
        register_post_type( 'links', $args );
    }
    add_action( 'init', 'links_post_type' );
}

/* Partners post type */
if( ! function_exists( 'partners_post_type' ) ) {
    function partners_post_type() {
        $labels = array(
            'name' => __( 'Partneri', 'Idvorsky' ),
            'singular_name' => __('Partneri', 'Idvorsky'),
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'partners',
            ),
        );
        register_post_type( 'partners', $args );
    }
    add_action( 'init', 'partners_post_type' );
}

/* Gallery post type */
if( ! function_exists( 'gallery_post_type' ) ) {
    function gallery_post_type() {
        $labels = array(
            'name' => __( 'Galerije', 'Idvorsky' ),
            'singular_name' => __('Galerije', 'Idvorsky'),
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'taxonomies'  => array( 'gallery_category', 'post_tag' ),
            'rewrite' => array(
                'slug' => 'gallery',
            ),
        );
        register_post_type( 'gallery', $args );
    }
    add_action( 'init', 'gallery_post_type' );
}

/* Documents post type */
if( ! function_exists( 'documents_post_type' ) ) {
    function documents_post_type() {
        $labels = array(
            'name' => __( 'Uputstva, formulari i dokumenta', 'Idvorsky' ),
            'singular_name' => __('Uputstva, formulari i dokumenta', 'Idvorsky'),
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'documents',
            ),
        );
        register_post_type( 'documents', $args );
    }
    add_action( 'init', 'documents_post_type' );
}

/* RiTT post type */
if( ! function_exists( 'ritt_post_type' ) ) {
    function ritt_post_type() {
        $labels = array(
            'name' => __( 'Evidencija izdatih potvrda o RiTT', 'Idvorsky' ),
            'singular_name' => __('Evidencija izdatih potvrda o RiTT', 'Idvorsky'),
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'public'       => true,
            'show_in_rest' => true,
            'labels' => $labels,
            'supports' => $supports,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'ritt',
            ),
        );
        register_post_type( 'ritt', $args );
    }
    add_action( 'init', 'ritt_post_type' );
}

/* RiTT number Meta Box */
if( ! function_exists( 'ritt_number_add_meta_box' ) ) {
    function ritt_number_add_meta_box() {
        add_meta_box(
            'ritt_number_box',
            'Broj',
            'ritt_number_meta_box_callback',
            'ritt'
        );
    }
    add_action( 'add_meta_boxes','ritt_number_add_meta_box' );
}
if( ! function_exists( 'ritt_number_meta_box_callback' ) ) {
    function ritt_number_meta_box_callback( $post ) {
        wp_nonce_field( 'ritt_number_meta_box_save_data','ritt_number_meta_box_nonce' );
        $value = get_post_meta( $post->ID,'_ritt_number_value_key',true );
        echo '<input type="text" id="ritt_number_field" name="ritt_number_field" value="' . esc_attr( $value ) . '" size="25" />';
    }
}
if( ! function_exists( 'ritt_number_meta_box_save_data' ) ) {
    function ritt_number_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['ritt_number_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['ritt_number_meta_box_nonce'],'ritt_number_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['ritt_number_field'] ) ) {
            return;
        }
        $data = sanitize_text_field( $_POST['ritt_number_field'] );
        update_post_meta( $post_id, '_ritt_number_value_key', $data );
    }
    add_action( 'save_post','ritt_number_meta_box_save_data' );
}

/* RiTT vrsta isprave Meta Box */
if( ! function_exists( 'ritt_vrsta_isprave_add_meta_box' ) ) {
    function ritt_vrsta_isprave_add_meta_box() {
        add_meta_box(
            'ritt_vrsta_isprave_box',
            'Vrsta isprave',
            'ritt_vrsta_isprave_meta_box_callback',
            'ritt'
        );
    }
    add_action( 'add_meta_boxes','ritt_vrsta_isprave_add_meta_box' );
}
if( ! function_exists( 'ritt_vrsta_isprave_meta_box_callback' ) ) {
    function ritt_vrsta_isprave_meta_box_callback( $post ) {
        wp_nonce_field( 'ritt_vrsta_isprave_meta_box_save_data','ritt_vrsta_isprave_meta_box_nonce' );
        $value = get_post_meta( $post->ID,'_ritt_vrsta_isprave_value_key',true );
        echo '<input type="text" id="ritt_vrsta_isprave_field" name="ritt_vrsta_isprave_field" value="' . esc_attr( $value ) . '" size="25" />';
    }
}
if( ! function_exists( 'ritt_vrsta_isprave_meta_box_save_data' ) ) {
    function ritt_vrsta_isprave_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['ritt_vrsta_isprave_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['ritt_vrsta_isprave_meta_box_nonce'],'ritt_vrsta_isprave_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['ritt_vrsta_isprave_field'] ) ) {
            return;
        }
        $data = sanitize_text_field( $_POST['ritt_vrsta_isprave_field'] );
        update_post_meta( $post_id, '_ritt_vrsta_isprave_value_key', $data );
    }
    add_action( 'save_post','ritt_vrsta_isprave_meta_box_save_data' );
}

/* RiTT vrsta opreme Meta Box */
if( ! function_exists( 'ritt_vrsta_opreme_add_meta_box' ) ) {
    function ritt_vrsta_opreme_add_meta_box() {
        add_meta_box(
            'ritt_vrsta_opreme_box',
            'Vrsta opreme',
            'ritt_vrsta_opreme_meta_box_callback',
            'ritt'
        );
    }
    add_action( 'add_meta_boxes','ritt_vrsta_opreme_add_meta_box' );
}
if( ! function_exists( 'ritt_vrsta_opreme_meta_box_callback' ) ) {
    function ritt_vrsta_opreme_meta_box_callback( $post ) {
        wp_nonce_field( 'ritt_vrsta_opreme_meta_box_save_data','ritt_vrsta_opreme_meta_box_nonce' );
        $value = get_post_meta( $post->ID,'_ritt_vrsta_opreme_value_key',true );
        echo '<input type="text" id="ritt_vrsta_opreme_field" name="ritt_vrsta_opreme_field" value="' . esc_attr( $value ) . '" size="25" />';
    }
}
if( ! function_exists( 'ritt_vrsta_opreme_meta_box_save_data' ) ) {
    function ritt_vrsta_opreme_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['ritt_vrsta_opreme_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['ritt_vrsta_opreme_meta_box_nonce'],'ritt_vrsta_opreme_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['ritt_vrsta_opreme_field'] ) ) {
            return;
        }
        $data = sanitize_text_field( $_POST['ritt_vrsta_opreme_field'] );
        update_post_meta( $post_id, '_ritt_vrsta_opreme_value_key', $data );
    }
    add_action( 'save_post','ritt_vrsta_opreme_meta_box_save_data' );
}

/* RiTT oznaka tipa/model Meta Box */
if( ! function_exists( 'ritt_oznaka_tipa_add_meta_box' ) ) {
    function ritt_oznaka_tipa_add_meta_box() {
        add_meta_box(
            'ritt_oznaka_tipa_box',
            'Oznaka tipa/model',
            'ritt_oznaka_tipa_meta_box_callback',
            'ritt'
        );
    }
    add_action( 'add_meta_boxes','ritt_oznaka_tipa_add_meta_box' );
}
if( ! function_exists( 'ritt_oznaka_tipa_meta_box_callback' ) ) {
    function ritt_oznaka_tipa_meta_box_callback( $post ) {
        wp_nonce_field( 'ritt_oznaka_tipa_meta_box_save_data','ritt_oznaka_tipa_meta_box_nonce' );
        $value = get_post_meta( $post->ID,'_ritt_oznaka_tipa_value_key',true );
        echo '<input type="text" id="ritt_oznaka_tipa_field" name="ritt_oznaka_tipa_field" value="' . esc_attr( $value ) . '" size="25" />';
    }
}
if( ! function_exists( 'ritt_oznaka_tipa_meta_box_save_data' ) ) {
    function ritt_oznaka_tipa_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['ritt_oznaka_tipa_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['ritt_oznaka_tipa_meta_box_nonce'],'ritt_oznaka_tipa_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['ritt_oznaka_tipa_field'] ) ) {
            return;
        }
        $data = sanitize_text_field( $_POST['ritt_oznaka_tipa_field'] );
        update_post_meta( $post_id, '_ritt_oznaka_tipa_value_key', $data );
    }
    add_action( 'save_post','ritt_oznaka_tipa_meta_box_save_data' );
}

/* RiTT datum izdavanja Meta Box */
if( ! function_exists( 'ritt_datum_izdavanja_add_meta_box' ) ) {
    function ritt_datum_izdavanja_add_meta_box() {
        add_meta_box(
            'ritt_datum_izdavanja_box',
            'Datum izdavanja',
            'ritt_datum_izdavanja_meta_box_callback',
            'ritt'
        );
    }
    add_action( 'add_meta_boxes','ritt_datum_izdavanja_add_meta_box' );
}
if( ! function_exists( 'ritt_datum_izdavanja_meta_box_callback' ) ) {
    function ritt_datum_izdavanja_meta_box_callback( $post ) {
        wp_nonce_field( 'ritt_datum_izdavanja_meta_box_save_data','ritt_datum_izdavanja_meta_box_nonce' );
        $value = get_post_meta( $post->ID,'_ritt_datum_izdavanja_value_key',true );
        echo '<input type="text" id="ritt_datum_izdavanja_field" name="ritt_datum_izdavanja_field" value="' . esc_attr( $value ) . '" size="25" />';
    }
}
if( ! function_exists( 'ritt_datum_izdavanja_meta_box_save_data' ) ) {
    function ritt_datum_izdavanja_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['ritt_datum_izdavanja_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['ritt_datum_izdavanja_meta_box_nonce'],'ritt_datum_izdavanja_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['ritt_datum_izdavanja_field'] ) ) {
            return;
        }
        $data = sanitize_text_field( $_POST['ritt_datum_izdavanja_field'] );
        update_post_meta( $post_id, '_ritt_datum_izdavanja_value_key', $data );
    }
    add_action( 'save_post','ritt_datum_izdavanja_meta_box_save_data' );
}

/* RiTT vazi do izdavanja Meta Box */
if( ! function_exists( 'ritt_vazi_do_add_meta_box' ) ) {
    function ritt_vazi_do_add_meta_box() {
        add_meta_box(
            'ritt_vazi_do_izdavanja_box',
            'Vazi do',
            'ritt_vazi_do_meta_box_callback',
            'ritt'
        );
    }
    add_action( 'add_meta_boxes','ritt_vazi_do_add_meta_box' );
}
if( ! function_exists( 'ritt_vazi_do_meta_box_callback' ) ) {
    function ritt_vazi_do_meta_box_callback( $post ) {
        wp_nonce_field( 'ritt_vazi_do_meta_box_save_data','ritt_vazi_do_meta_box_nonce' );
        $value = get_post_meta( $post->ID,'_ritt_vazi_do_value_key',true );
        echo '<input type="text" id="ritt_vazi_do_field" name="ritt_vazi_do_field" value="' . esc_attr( $value ) . '" size="25" />';
    }
}
if( ! function_exists( 'ritt_vazi_do_meta_box_save_data' ) ) {
    function ritt_vazi_do_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['ritt_vazi_do_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['ritt_vazi_do_meta_box_nonce'],'ritt_vazi_do_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['ritt_vazi_do_field'] ) ) {
            return;
        }
        $data = sanitize_text_field( $_POST['ritt_vazi_do_field'] );
        update_post_meta( $post_id, '_ritt_vazi_do_value_key', $data );
    }
    add_action( 'save_post','ritt_vazi_do_meta_box_save_data' );
}

/* Testimonials Meta Box */
if( ! function_exists( 'testimonials_add_meta_box' ) ) {
    function testimonials_add_meta_box() {
        add_meta_box(
            'testimonials_box',
            'Position',
            'testimonials_meta_box_callback',
            'testimonials'
        );
    }
    add_action( 'add_meta_boxes','testimonials_add_meta_box' );
}
if( ! function_exists( 'testimonials_meta_box_callback' ) ) {
    function testimonials_meta_box_callback( $post ) {
        wp_nonce_field( 'testimonials_meta_box_save_data','testimonials_meta_box_nonce' );
        $value = get_post_meta( $post->ID,'_testimonials_value_key',true );
        echo '<input type="text" id="testimonials_field" name="testimonials_field" value="' . esc_attr( $value ) . '" size="25" />';
    }
}
if( ! function_exists( 'testimonials_meta_box_save_data' ) ) {
    function testimonials_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['testimonials_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['testimonials_meta_box_nonce'],'testimonials_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['testimonials_field'] ) ) {
            return;
        }
        $data = sanitize_text_field( $_POST['testimonials_field'] );
        update_post_meta( $post_id, '_testimonials_value_key', $data );
    }
    add_action( 'save_post','testimonials_meta_box_save_data' );
}

/* Lab people Meta Box */
if( ! function_exists( 'lab_people_add_meta_box' ) ) {
    function lab_people_add_meta_box() {
        add_meta_box(
            'lab_people_box',
            'Informations',
            'lab_people_box_meta_box_callback',
            'lab_people'
        );
    }
    add_action( 'add_meta_boxes','lab_people_add_meta_box' );
}
if( ! function_exists( 'lab_people_box_meta_box_callback' ) ) {
    function lab_people_box_meta_box_callback( $post ) {
        wp_nonce_field( 'lab_people_meta_box_save_data','lab_people_meta_box_nonce' );
        $position = get_post_meta( $post->ID,'_position_value_key',true );
        echo '<label for="position_field">Position: </label>';
        echo '<input type="text" id="position_field" name="position_field" value="' . esc_attr( $position ) . '" size="25" />';
        echo '<br>';
        $education = get_post_meta( $post->ID,'_education_value_key',true );
        echo '<label for="education_field">Education: </label>';
        echo '<input type="text" id="education_field" name="education_field" value="' . esc_attr( $education ) . '" size="25" />';
    }
}
if( ! function_exists( 'lab_people_meta_box_save_data' ) ) {
    function lab_people_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['lab_people_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['lab_people_meta_box_nonce'],'lab_people_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['position_field'] ) ) {
            return;
        }
        $data = sanitize_text_field( $_POST['position_field'] );
        $data1 = sanitize_text_field( $_POST['education_field'] );
        update_post_meta( $post_id, '_position_value_key', $data );
        update_post_meta( $post_id, '_education_value_key', $data1 );
    }
    add_action( 'save_post','lab_people_meta_box_save_data' );
}

/* Links Meta Box */
if( ! function_exists( 'links_add_meta_box' ) ) {
    function links_add_meta_box() {
        add_meta_box(
            'links_box',
            'Link',
            'links_meta_box_callback',
            'links'
        );
    }
    add_action( 'add_meta_boxes','links_add_meta_box' );
}
if( ! function_exists( 'links_meta_box_callback' ) ) {
    function links_meta_box_callback( $post ) {
        wp_nonce_field( 'links_meta_box_save_data','links_meta_box_nonce' );
        $value = get_post_meta( $post->ID,'_links_value_key',true );
        echo '<input type="text" id="links_field" name="links_field" value="' . esc_attr( $value ) . '" size="25" />';
    }
}
if( ! function_exists( 'links_meta_box_save_data' ) ) {
    function links_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['links_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['links_meta_box_nonce'],'links_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['links_field'] ) ) {
            return;
        }
        $data = sanitize_text_field( $_POST['links_field'] );
        update_post_meta( $post_id, '_links_value_key', $data );
    }
    add_action( 'save_post','links_meta_box_save_data' );
}

/* Partners Meta Box */
if( ! function_exists( 'partners_add_meta_box' ) ) {
    function partners_add_meta_box() {
        add_meta_box(
            'partners_box',
            'Link',
            'partners_meta_box_callback',
            'partners'
        );
    }
    add_action( 'add_meta_boxes','partners_add_meta_box' );
}
if( ! function_exists( 'partners_meta_box_callback' ) ) {
    function partners_meta_box_callback( $post ) {
        wp_nonce_field( 'partners_meta_box_save_data','partners_meta_box_nonce' );
        $value = get_post_meta( $post->ID,'_partners_value_key',true );
        echo '<input type="text" id="partners_field" name="partners_field" value="' . esc_attr( $value ) . '" size="25" />';
    }
}
if( ! function_exists( 'partners_meta_box_save_data' ) ) {
    function partners_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['partners_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['partners_meta_box_nonce'],'partners_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['partners_field'] ) ) {
            return;
        }
        $data = sanitize_text_field( $_POST['partners_field'] );
        update_post_meta( $post_id, '_partners_value_key', $data );
    }
    add_action( 'save_post','partners_meta_box_save_data' );
}

/* Informations Meta Box */
if( ! function_exists( 'informations_add_meta_box' ) ) {
    function informations_add_meta_box() {
        global $post;
        $page = get_post_meta( $post->ID,'_wp_page_template',true );
        if( $page == 'template-contact-ask-us.php' || $page == 'template-contact-informations.php' ) {
            add_meta_box(
                'informations_box',
                'Informations',
                'informations_meta_box_callback',
                'page'
            );
        }
    }
    add_action( 'add_meta_boxes','informations_add_meta_box' );
}
if( ! function_exists( 'informations_meta_box_callback' ) ) {
    function informations_meta_box_callback( $post ) {
        wp_nonce_field( 'informations_meta_box_save_data','informations_meta_box_nonce' );
        $value = get_post_meta( $post->ID,'_informations_value_key',true );
        wp_editor( $value, 'informations_field' );
    }
}
if( ! function_exists( 'informations_meta_box_save_data' ) ) {
    function informations_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['informations_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['informations_meta_box_nonce'],'informations_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['informations_field'] ) ) {
            return;
        }
        if( isset( $_POST['informations_field'] ) ) {
            update_post_meta( $post_id,'_informations_value_key',$_POST['informations_field'] );
        }
    }
    add_action( 'save_post','informations_meta_box_save_data' );
}

/* Map Meta Box */
if( ! function_exists( 'map_add_meta_box' ) ) {
    function map_add_meta_box() {
        global $post;
        $page = get_post_meta( $post->ID,'_wp_page_template',true );
        if( $page == 'template-contact-informations.php' ) {
            add_meta_box(
                'map_box',
                'Map',
                'map_meta_box_callback',
                'page'
            );
        }
    }
    add_action( 'add_meta_boxes','map_add_meta_box' );
}
if( ! function_exists( 'map_meta_box_callback' ) ) {
    function map_meta_box_callback( $post ) {
        wp_nonce_field( 'map_meta_box_save_data','map_meta_box_nonce' );
        $latitude = get_post_meta( $post->ID,'_latitude_value_key',true );
        echo '<label for="latitude_field">Latitude: </label>';
        echo '<input type="text" id="latitude_field" name="latitude_field" value="' . esc_attr( $latitude ) . '" size="25" />';
        echo '<br>';
        $longitude = get_post_meta( $post->ID,'_longitude_value_key',true );
        echo '<label for="longitude_field">Longitude: </label>';
        echo '<input type="text" id="longitude_field" name="longitude_field" value="' . esc_attr( $longitude ) . '" size="25" />';
    }
}
if( ! function_exists( 'map_meta_box_save_data' ) ) {
    function map_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['map_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['map_meta_box_nonce'],'map_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['informations_field'] ) ) {
            return;
        }
        $data = sanitize_text_field( $_POST['latitude_field'] );
        $data1 = sanitize_text_field( $_POST['longitude_field'] );
        update_post_meta( $post_id, '_latitude_value_key', $data );
        update_post_meta( $post_id, '_longitude_value_key', $data1 );
    }
    add_action( 'save_post','map_meta_box_save_data' );
}

/* Videos Meta Box */
if( ! function_exists( 'videos_add_meta_box' ) ) {
    function videos_add_meta_box() {
        add_meta_box(
            'videos_box',
            'Video',
            'videos_meta_box_callback',
            'gallery'
        );
    }
    add_action( 'add_meta_boxes','videos_add_meta_box' );
}
if( ! function_exists( 'videos_meta_box_callback' ) ) {
    function videos_meta_box_callback( $post ) {
        wp_nonce_field( 'videos_meta_box_save_data','videos_meta_box_nonce' );
        $value = get_post_meta( $post->ID,'_videos_value_key',true );
        echo '<input type="text" id="videos_field" name="videos_field" value="' . esc_attr( $value ) . '" size="25" />';
    }
}
if( ! function_exists( 'videos_meta_box_save_data' ) ) {
    function videos_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['videos_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['videos_meta_box_nonce'],'videos_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['videos_field'] ) ) {
            return;
        }
        $data = sanitize_text_field( $_POST['videos_field'] );
        update_post_meta( $post_id, '_videos_value_key', $data );
    }
    add_action( 'save_post','videos_meta_box_save_data' );
}

/* Pdf meta box */
if( ! function_exists( 'pdf_add_meta_box' ) ) {
    function pdf_add_meta_box() {
        add_meta_box(
            'pdf_box',
            'Upload PDF',
            'pdf_meta_box_callback',
            'documents'
        );
    }
    add_action('add_meta_boxes', 'pdf_add_meta_box');
}
if( ! function_exists('pdf_meta_box_callback') ) {
    function pdf_meta_box_callback() {
        wp_nonce_field(plugin_basename(__FILE__), 'pdf_meta_box_nonce');
        $html = '<p class="description">';
        $html .= 'Upload your PDF here.';
        $html .= '</p>';
        $html .= '<input type="file" id="pdf_field" name="pdf_field" value="" size="25">';
        $filearray = get_post_meta( get_the_ID(), '_pdf_value_key', true );
        $this_file = '';
        if(isset($filearray['url'])) {
            $this_file = $filearray['url'];
        }
        if($this_file != ""){
           $html .= '<div>Current file:<br>"' . $this_file . '"</div>';
        }
        echo $html;
    }
}
if( ! function_exists( 'pdf_meta_box_save_data' ) ) {
    function pdf_meta_box_save_data($id) {
        if(!empty($_FILES['pdf_field']['name'])) {
            $supported_types = array('application/pdf');
            $arr_file_type = wp_check_filetype(basename($_FILES['pdf_field']['name']));
            $uploaded_type = $arr_file_type['type'];

            if(in_array($uploaded_type, $supported_types)) {
                $upload = wp_upload_bits($_FILES['pdf_field']['name'], null, file_get_contents($_FILES['pdf_field']['tmp_name']));
                if(isset($upload['error']) && $upload['error'] != 0) {
                    wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
                } else {
                    update_post_meta($id, '_pdf_value_key', $upload);
                }
            }
            else {
                wp_die("The file type that you've uploaded is not a PDF.");
            }
        }
    }
    add_action('save_post', 'pdf_meta_box_save_data');
}

/* Documents Meta Box */
if( ! function_exists( 'documents_add_meta_box' ) ) {
    function documents_add_meta_box() {
        add_meta_box(
            'documents_box',
            'Upload document',
            'documents_meta_box_callback',
            'documents'
        );
    }
    add_action( 'add_meta_boxes','documents_add_meta_box' );
}
if( ! function_exists( 'documents_meta_box_callback' ) ) {
    function documents_meta_box_callback( $post ) {
        wp_nonce_field( 'documents_meta_box_save_data','documents_meta_box_nonce' );
        $value = get_post_meta( $post->ID,'_documents_value_key',true );
        echo '<input type="text" id="documents_field" name="documents_field" value="' . esc_attr( $value ) . '" size="25" />';
    }
}
if( ! function_exists( 'documents_meta_box_save_data' ) ) {
    function documents_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['documents_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['documents_meta_box_nonce'],'documents_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['documents_field'] ) ) {
            return;
        }
        $data = sanitize_text_field( $_POST['documents_field'] );
        update_post_meta( $post_id, '_documents_value_key', $data );
    }
    add_action( 'save_post','documents_meta_box_save_data' );
}

/* Update edit form */
if( ! function_exists( 'update_edit_form' ) ) {
    function update_edit_form() {
        echo ' enctype="multipart/form-data"';
    }
    add_action('post_edit_form_tag', 'update_edit_form');
}

function create_category_taxonomy() {
	$labels = array(
		'name'              => _x( 'Category', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Category', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Category', 'textdomain' ),
		'all_items'         => __( 'All Category', 'textdomain' ),
		'parent_item'       => __( 'Parent Category', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Category:', 'textdomain' ),
		'edit_item'         => __( 'Edit Category', 'textdomain' ),
		'update_item'       => __( 'Update Category', 'textdomain' ),
		'add_new_item'      => __( 'Add New Category', 'textdomain' ),
		'new_item_name'     => __( 'New Category Name', 'textdomain' ),
		'menu_name'         => __( 'Category', 'textdomain' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'gallery_category' ),
	);
	register_taxonomy( 'gallery_category', array( 'gallery' ), $args );

}
add_action( 'init', 'create_category_taxonomy');

function create_category2_taxonomy() {
	$labels = array(
		'name'              => _x( 'Pages', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Pages', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Pages', 'textdomain' ),
		'all_items'         => __( 'All Pages', 'textdomain' ),
		'parent_item'       => __( 'Parent Pages', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Pages:', 'textdomain' ),
		'edit_item'         => __( 'Edit Pages', 'textdomain' ),
		'update_item'       => __( 'Update Pages', 'textdomain' ),
		'add_new_item'      => __( 'Add New Pages', 'textdomain' ),
		'new_item_name'     => __( 'New Pages Name', 'textdomain' ),
		'menu_name'         => __( 'Pages', 'textdomain' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'documents_category' ),
	);
	register_taxonomy( 'documents_category', array( 'documents' ), $args );

}
add_action( 'init', 'create_category2_taxonomy' );

function create_category3_taxonomy() {
	$labels = array(
		'name'              => _x( 'Pages', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Pages', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Pages', 'textdomain' ),
		'all_items'         => __( 'All Pages', 'textdomain' ),
		'parent_item'       => __( 'Parent Pages', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Pages:', 'textdomain' ),
		'edit_item'         => __( 'Edit Pages', 'textdomain' ),
		'update_item'       => __( 'Update Pages', 'textdomain' ),
		'add_new_item'      => __( 'Add New Pages', 'textdomain' ),
		'new_item_name'     => __( 'New Pages Name', 'textdomain' ),
		'menu_name'         => __( 'Pages', 'textdomain' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'ritt_category' ),
	);
	register_taxonomy( 'ritt_category', array( 'ritt' ), $args );

}
add_action( 'init', 'create_category3_taxonomy' );

function add_excerpt_for_testimonials_post_type() {
    add_post_type_support( 'testimonials', 'excerpt' );
}
add_action( 'init', 'add_excerpt_for_testimonials_post_type' );
