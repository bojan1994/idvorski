<?php
if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
    ?>
    <div class="box">
        <div class="title ask">
            <a href="<?php echo get_theme_mod( 'ask_us_text3_en' ); ?>">
                <img src="<?php bloginfo('template_url'); ?>/img/pitajte_nas.en.png" width="327" height="41" alt="<?php echo get_theme_mod( 'ask_us_text' ); ?>"/>
            </a>
        </div>
        <div class="text_section">
            <div class="arrow_brown">
            	<a href="<?php echo get_theme_mod( 'ask_us_text3_en' ); ?>" class="small_arrow"></a>
            </div>
            <div class="text_box ask-us-box">
                <div style="height:60px;">
                    <a href="<?php echo get_theme_mod( 'ask_us_text3_en' ); ?>">
                        <p><?php echo get_theme_mod( 'ask_us_text2_en' ); ?></p>
                    </a>
                </div>
                <img src="<?php bloginfo('template_url'); ?>/img/home_ranfla.png" alt="Ranfla" style="margin:20px 0 0 4px;">
            </div>
        </div>
    </div>
    <?php
} else {
    ?>
    <div class="box">
        <div class="title ask">
            <a href="<?php echo get_theme_mod( 'ask_us_text3' ); ?>">
                <img src="<?php bloginfo('template_url'); ?>/img/pitajte_nas.rs.png" width="327" height="41" alt="<?php echo get_theme_mod( 'ask_us_text' ); ?>"/>
            </a>
        </div>
        <div class="text_section">
            <div class="arrow_brown">
            	<a href="<?php echo get_theme_mod( 'ask_us_text3' ); ?>" class="small_arrow"></a>
            </div>
            <div class="text_box ask-us-box">
                <div style="height:60px;">
                    <a href="<?php echo get_theme_mod( 'ask_us_text3' ); ?>">
                        <p><?php echo get_theme_mod( 'ask_us_text2' ); ?></p>
                    </a>
                </div>
                <img src="<?php bloginfo('template_url'); ?>/img/home_ranfla.png" alt="Ranfla" style="margin:20px 0 0 4px;">
            </div>
        </div>
    </div>
    <?php
}
?>
