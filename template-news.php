<?php

/* Template name: Vesti */

get_header(); ?>

<div id="content" style="position:relative;">
    <?php
    get_template_part( 'content', 'top-image' );
    get_template_part( 'content', 'all-news' );
    get_template_part( 'content', 'side-news-news' );
    get_template_part( 'content', 'side-news-archive' );
    ?>
</div>
<div style="clear:both;"></div>

<?php get_footer();
