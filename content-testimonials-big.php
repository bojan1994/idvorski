<div class="left_section">
    <div class="osoblje" >
        <?php wp_title( '', true, '' ); ?>
    </div>
    <?php
    $args = array(
        'post_type' => 'testimonials',
    );
    $query = new WP_Query( $args );
    if( $query->have_posts() ) :
        while( $query->have_posts() ) :
            $query->the_post();
            ?>
            <div class="links">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border:none">
                            <div class="news_image_new"></div>
                        </td>
                        <td style="border:none">
                            <div class="links_description">
                                <div class="text_links">
                                	<span style="font-family:Arial, Helvetica, sans-serif; font-size:18px" >
                                        <a href="<?php the_permalink(); ?>" >
                                            <?php the_title(); ?>
                                        </a>
                                        <div style="font-size:12px;">
    										<?php echo get_post_meta( $post->ID, '_testimonials_value_key', true ); ?>
                                        </div>
                                        <div style="font-size:10px;">
                                            <a  href="rs/testimonials/3/John-Davies.html" style="text-decoration:none; color:#919191;padding:0; font-size:10px ">
                                                <?php the_excerpt(); ?>
                                            </a>
                                        </div>
                                 	</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <?php
        endwhile;
        wp_reset_postdata();
    endif;
    ?>
</div>
