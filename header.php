<!DOCTYPE html>
<html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?> onload="search_field_page_load()" data-baseurlforajax="<?php echo get_template_directory_uri(); ?>">

        <div id="container">

            <div id="header">

                <div class="logo">
                    <a href="<?php echo home_url(); ?>">
                        <?php
                        if( isset($_GET['lang']) && $_GET['lang'] == 'en' ) {
                            $custom_logo = get_theme_mod( 'logo_en', array( 'class' => 'img-responsive' ) );
                            ?>
                            <img src="<?php echo $custom_logo; ?>" alt="<?php bloginfo( 'name' ); ?>">
                            <?php
                        } else {
                            $custom_logo = get_theme_mod( 'custom_logo', array( 'class' => 'img-responsive' ) );
                            $image = wp_get_attachment_image_src( $custom_logo, 'full' );
                            ?>
                            <img src="<?php echo $image[0]; ?>" alt="<?php bloginfo( 'name' ); ?>">
                            <?php
                        }
                        ?>
                    </a>
                </div>

                <div class="header_blue">
                    <?php
                    if(isset($_GET['lang']) && $_GET['lang'] == 'en') {
                        ?>
                        <img src="<?php bloginfo('template_url'); ?>/img/header_ranfla_iznad-eu-en.png" alt="Idvorsky Laboratorije" />
                        <?php
                    } else {
                        ?>
                        <img src="<?php bloginfo('template_url'); ?>/img/header_ranfla_iznad-eu-rs.png" alt="Idvorsky Laboratorije" />
                        <?php
                    }
                    ?>
                </div>

                <div id="menu" style="width:844px">
                    <menu>
                        <?php
                        $args = array(
                            'theme_location' => 'primary',
                        );
                        wp_nav_menu( $args );
                        ?>
                    </menu>
                </div>

                <div id="languages" style="width:183px">
                    <?php
                    $args2 = array(
                        'theme_location' => 'languages',
                    );
                    wp_nav_menu( $args2 );
                    ?>
                </div>

            </div>
