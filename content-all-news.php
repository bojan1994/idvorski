<div class="left_section">
    <div class="osoblje" style="font-family: Arial, Helvetica, sans-serif;">
   		<?php wp_title( '', true, '' ); ?>
    </div>
    <?php
    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => 6,
        'paged' => $paged,
    );
    $query = new WP_Query( $args );
    if( $query->have_posts() ) :
        while( $query->have_posts() ) :
            $query->the_post();
            ?>
            <div class="links">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border:none">
                            <div class="news_image_new">
                                 <a href="<?php the_permalink(); ?>" >
                                     <?php
                                     $post_thumbnail_id = get_post_thumbnail_id();
                                     $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
                                     ?>
                                     <img src="<?php echo $post_thumbnail_url; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" width="135" />
                                 </a>
                            </div>
                        </td>
                        <td style="border:none">
                            <div class="links_description">
                                <div class="text_links">
                                	<span style="font-family:Arial, Helvetica, sans-serif; font-size:18px" >
                                        <a href="<?php the_permalink(); ?>" >
                                            <?php the_title(); ?>
                                        </a>
                                        <div style="font-size:10px;">
                                            <div style="font-size:10px">
                                                <?php echo get_the_date( 'd.m.Y.' ); ?>
                                            </div>
                                            <a href="<?php the_permalink(); ?>" style="text-decoration:none; color:#919191;padding:0; font-size:10px ">
                                                <?php the_excerpt(); ?>
                                            </a>
                                        </div>
                                 	</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <div style="clear:both;"></div>
            </div>
            <?php
        endwhile;
        ?>
        <div class="news">
            <div class="pagination">
                <?php
                $big = 999999999;
                echo paginate_links( array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'format' => '?paged=%#%',
                    'current' => max( 1, get_query_var( 'paged' ) ),
                    'total' => $query->max_num_pages,
                    'prev_text' => __( '«' ),
    	            'next_text' => __( '»' ),
                ));
                ?>
            </div>
        </div>
        <?php
        wp_reset_postdata();
    endif;
    ?>
</div>
